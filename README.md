# education-frontend-app-infra

Deploy repo for demo education-frontend-app (angular).

## Handle frontend environment variables
Education-frontend-app handles environment variables based on these ideas:
https://medium.com/voobans-tech-stories/multiple-environments-with-angular-and-docker-2512e342ab5a

The file /usr/share/nginx/html/assets/environments/environment.json will be overwritten with json structure
from cm.yaml that matches the overlay for the target environment.